import requests
import json

class CarNotFoundError(Exception):
    pass

def ask_user_for_numberplate():
    return 'PV574D'

def get_catalog_price(num_plate):
    url = 'https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=' + num_plate
    print(url)
    data = get_rdw_data(url)
    if data:
        car_data = data[0]
        if 'catalogusprijs' in car_data:
            price = car_data['catalogusprijs']
            return price
        else:
            raise ValueError("Catalogus prijs niet in de data aanwezig")
    else:
        CarNotFoundError(str(num_plate) + " niet gevonden.")

def get_rdw_data(url):
    response = requests.get(url)

    if response.status_code == 200:  # successfull response
        data = json.loads(response.text)  # convert to python dict
        return data
    else:
        raise Exception("Server levert geen data")

def present_catalog_price(price):
    print("De catalogusprijs is", price)


def main():
    try:
        num_plate = ask_user_for_numberplate()
        price = get_catalog_price(num_plate)
        present_catalog_price(price)
    except Exception as e:
        print("Kon helaas geen data ophalen")
        print(e)

main()