import sys
import argparse
import time
import requests
from pythonping import ping
import random


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('ip', type=str, help="ip of the server")
    parser.add_argument('-p', '--port', action="store", type=int, help="server port")
    parser.add_argument('-i', '--interval', action="store", type=int, help="interval between pings")
    args = parser.parse_args()
    return args

def get_ping_address(ip, port):
    print("GET request op ", ip, port)
    url = 'http://{ip}:{port}/api/pingip'.format(ip=ip,
                                                 port=port)
    response = requests.get(url)
    data = response.json()
    if 'ip' in data:
        return data['ip']
    else:
        raise ValueError("geen ping ip in rest data gevonden.")

def do_ping(ping_ip):
    print("Ping actie op", ping_ip)
    ping_response = int(random.gauss(300, 100))
    if ping_response < 1:
        return 1
    return ping_response
    #ping_response = ping(ping_ip, count=5)
    #return ping_response.rtt_avg_ms


def post_ping_time(ip, port, ping_time):
    print("POST request op", ip, port, "van ping time", ping_time)
    payload = {'ping_time': ping_time,
               'source_ip': '194.109.45.7'
    }

    url = 'http://{ip}:{port}/api/pingtime'.format(ip=ip,
                                                 port=port)

    response = requests.post(url, json=payload)

def run_ping_loop(ip, port, interval):
    while True:
        ping_ip = get_ping_address(ip, port)
        ping_time = do_ping(ping_ip)
        post_ping_time(ip, port, ping_time)
        time.sleep(interval)

def main():
    print("ping_client")
    args = parse_arguments()
    interval = 10  # seconds
    if args.interval:
        interval = args.interval

    port = 5000
    if args.port:
        port = args.port

    run_ping_loop(args.ip, port, interval)
    sys.exit(0)

if __name__ == "__main__":
    main()