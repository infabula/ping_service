from flask import Flask, render_template
from flask_restful import Resource, Api, reqparse, abort

app = Flask(__name__)
api = Api(app)

ping_results = {}

# Web-pagina's
@app.route('/')
def index_page():
    return 'Ping service'

@app.route('/pingtimes/<ip>')
def show_ping_times(ip):
    if ip in ping_results:
        latest_pingtimes = ping_results[ip][-10:]
        return render_template('ping_list.j2.html',
                                ip=ip, pingtimes=latest_pingtimes)
    else:
        return "onbekend ip"

# REST API


class PingIP(Resource):
    def get(self):
        return {'ip': "6.6.6.6"}  # fixture

class PingTime(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('source_ip', type=str, help="source ip address")
        parser.add_argument('ping_time', type=int, help='ping time')
        args = parser.parse_args()
        print("source_ip", args.source_ip)
        print('ping_time', args.ping_time)

        if not args.source_ip in ping_results:
            ping_results[args.source_ip] = []
        ping_results[args.source_ip].append(args.ping_time)
        print(ping_results[args.source_ip])

api.add_resource(PingIP, '/api/pingip')
api.add_resource(PingTime, '/api/pingtime')