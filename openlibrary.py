import json
import requests

subject = "peace"
url = 'https://openlibrary.org/subjects/' + subject + ".json"

response = requests.get(url)

# test for success
if response.status_code == 200:  # successfull response
    print(response.text)  # json string
    data = json.loads(response.text) # convert to python dict
    print(data) # print dictionary
else:
    print("Could not fetch data from", url)
